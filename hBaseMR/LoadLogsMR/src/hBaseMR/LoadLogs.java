package hBaseMR;

import java.io.IOException;

import java.io.BufferedReader;
import java.io.FileReader;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
//import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.conf.Configuration;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LoadLogs {

	static final Pattern patt = Pattern.compile("^(\\S+) - - \\[(\\S+) [+-]\\d+\\] \"[A-Z]+ (\\S+) HTTP/\\d\\.\\d\" \\d+ (\\d+)$");
	static SimpleDateFormat dateparse = new SimpleDateFormat("dd/MMM/yyyy:HH:mm:ss");
	
	public static Put get_put (String line) throws ParseException {
		
		byte[] rowkey = DigestUtils.md5(line);
		Put p = new Put(rowkey);
	    p.addColumn(Bytes.toBytes("raw"), Bytes.toBytes("line"), Bytes.toBytes(line));
	    Matcher m = patt.matcher(line);
	    if (m.find()){
		    p.addColumn(Bytes.toBytes("struct"), Bytes.toBytes("host"),Bytes.toBytes(m.group(1)));
		    
		    Date date = dateparse.parse(m.group(2));
		    long unixTime = date.getTime();
//		    System.out.println(date.getTime());
		    p.addColumn(Bytes.toBytes("struct"), Bytes.toBytes("date"),Bytes.toBytes(unixTime));
		    
		    p.addColumn(Bytes.toBytes("struct"), Bytes.toBytes("path"),Bytes.toBytes(m.group(3)));
		    
		    long size = Long.parseLong(m.group(4));
		    p.addColumn(Bytes.toBytes("struct"), Bytes.toBytes("bytes"),Bytes.toBytes(size));
	    	
	    }
	    return p;
		
	}

	
	public static void main (String [] args) throws IOException, ParseException
	{	
		Configuration config = HBaseConfiguration.create();
//		Connection connection = ConnectionFactory.createConnection();
		Connection connection = ConnectionFactory.createConnection(config);
		Table table = connection.getTable(TableName.valueOf(args[0]));
		
		String inputLine = "";
		BufferedReader br = new BufferedReader(new FileReader(args[1]));

		while ((inputLine=br.readLine())!=null){
			inputLine = br.readLine();
			table.put(get_put(inputLine));
		}
		
		br.close();
		table.close();
		connection.close();
		 
	}

}
