package hBaseMR;
import java.io.IOException;
//setenv HADOOP_CLASSPATH `hadoop classpath`:`hbase classpath`:correlateLogs.jar

import java.lang.Math;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;

import org.apache.hadoop.io.LongWritable;
//import org.apache.hadoop.io.DoubleWritable;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
//import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
//import org.apache.hadoop.mapreduce.lib.chain.*;
import org.apache.hadoop.mapreduce.lib.chain.ChainReducer;


import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


//import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.*;
import java.util.regex.Pattern;

//import java.text.ParseException;
import java.text.SimpleDateFormat;
//import java.util.Date;
//import org.apache.hadoop.hbase.TableName;
//import java.util.regex.Matcher;
//import org.apache.hadoop.hbase.util.Bytes;
//import org.apache.commons.codec.digest.DigestUtils;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;

public class CorrelateLogs extends Configured implements Tool{
	static final Pattern patt = Pattern.compile("^(\\S+) - - \\[(\\S+) [+-]\\d+\\] \"[A-Z]+ (\\S+) HTTP/\\d\\.\\d\" \\d+ (\\d+)$");
	static SimpleDateFormat dateparse = new SimpleDateFormat("dd/MMM/yyyy:HH:mm:ss");
	
	public static class hBaseMapper extends TableMapper <Text, LongPairWritable>{
		private LongPairWritable pair = new LongPairWritable();
		private long one = 1;
		@Override
		public void map(ImmutableBytesWritable raw, Result value, Context context) throws IOException, InterruptedException {
			if ((value.getValue(Bytes.toBytes("struct"), Bytes.toBytes("host"))!=null&&(value.getValue(Bytes.toBytes("struct"), Bytes.toBytes("bytes"))!=null)))
			{
				byte [] host = value.getValue(Bytes.toBytes("struct"), Bytes.toBytes("host"));
				byte [] bytes = value.getValue(Bytes.toBytes("struct"), Bytes.toBytes("bytes"));
				pair.set(one, Bytes.toLong(bytes));
				Text key= new Text();
				key.set(Bytes.toString(host));
				context.write(key, pair);
			}
		}
	}
	public static class chainMapper extends Mapper <Text, LongPairWritable, Text, Text>{
		long n, sumX, sumY, sumXY, sumXSquared, sumYSquared;
		double r;
		String output="";
		Text Output = new Text();
		Text Key = new Text();
		@Override
		public void setup(Context context){
			// x is the number of request made
			// y is the number of bytes
			n = 0;
			sumX=0;
			sumY=0;
			sumXY=0;
			sumXSquared=0;
			sumYSquared=0;
		}
		@Override
		public void map(Text key, LongPairWritable value, Context context) throws IOException, InterruptedException {
			n+=1;
			sumX+=value.get_0();
			sumY+=value.get_1();
			sumXY+=value.get_0()*value.get_1();
			sumXSquared+=value.get_0()*value.get_0();
			sumYSquared+=value.get_1()*value.get_1();
			
		}
		
		@Override
		public void cleanup(Context context) throws IOException, InterruptedException{
			r=((double)n*sumXY-(double)sumX*(double) sumY)/(Math.sqrt((double)n*(double)sumXSquared-(Math.pow((double)sumX,2)))*Math.sqrt((double) n*(double) sumYSquared-(Math.pow((double) sumY,2))));
			output = "n " + Long.toString (n) + "\n Sx " + Long.toString(sumX) + "\n Sx2 " + Long.toString(sumXSquared) + "\n Sy " + Long.toString(sumY) + "\n Sy2 " + Long.toString(sumYSquared)+ "\n Sxy " + Long.toString(sumXY)+ "\n r " + Double.toString(r)+"\n r2 " + Double.toString(r*r);
			Output.set(output);
			Key.set("");
			context.write(Key, Output);
		}
	}
    
	public static class cReducer extends Reducer <Text, LongPairWritable, Text, LongPairWritable> {
		private LongPairWritable result = new LongPairWritable();
		@Override
		public void reduce(Text key, Iterable<LongPairWritable> pairs, Context context) throws IOException, InterruptedException {
			long sum_0=0;
			long sum_1=0;
			for (LongPairWritable val: pairs){
				sum_0+=val.get_0();
				sum_1+=val.get_1();
			}
			result.set(sum_0, sum_1);
			context.write(key, result);
		}
		
	}
	
	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new CorrelateLogs(), args);
		System.exit(res);
	}

	@Override
	public int run(String[] args) throws Exception {
//		Configuration conf = HBaseConfiguration.create();
		Configuration conf = this.getConf();

		Job job = Job.getInstance(conf, "calculate correlate by bslc");
		job.setJarByClass(CorrelateLogs.class);
		
		job.setMapperClass(hBaseMapper.class);
		

		 ChainReducer.setReducer(job, cReducer.class, Text.class, LongPairWritable.class, Text.class, LongPairWritable.class, new Configuration(false));
		 ChainReducer.addMapper(job, chainMapper.class, Text.class, LongPairWritable.class,  Text.class, Text.class, new Configuration(false));

		job.setInputFormatClass(TextInputFormat.class);
		
		
//		job.setReducerClass(chainReducer.class);

//		job.setMapOutputKeyClass(Text.class);
//      job.setMapOutputValueClass(LongPairWritable.class);
		
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(LongWritable.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		TextInputFormat.addInputPath(job, new Path(args[0]));
		TextOutputFormat.setOutputPath(job, new Path(args[2]));
		Scan scan = new Scan();
		TableMapReduceUtil.addDependencyJars(job);
		TableMapReduceUtil.initTableMapperJob(args[1], scan, hBaseMapper.class,Text.class, LongPairWritable.class,job);
//		TableMapReduceUtil.initTableReducerJob(args[1], chainReducer.class, job);
		job.setNumReduceTasks(1);

		return job.waitForCompletion(true) ? 0 : 1;
	}

}
