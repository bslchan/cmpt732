package hBaseMR;
import java.io.IOException;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;

import org.apache.hadoop.io.LongWritable;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
//import org.apache.hadoop.mapreduce.Mapper;

import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.*;
import java.util.regex.Pattern;

import java.text.ParseException;
import java.text.SimpleDateFormat;
//import java.util.Date;
//import org.apache.hadoop.hbase.TableName;
//import java.util.regex.Matcher;
//import org.apache.hadoop.hbase.util.Bytes;
//import org.apache.commons.codec.digest.DigestUtils;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;

public class LoadLogsMR extends Configured implements Tool{
	static final Pattern patt = Pattern.compile("^(\\S+) - - \\[(\\S+) [+-]\\d+\\] \"[A-Z]+ (\\S+) HTTP/\\d\\.\\d\" \\d+ (\\d+)$");
	static SimpleDateFormat dateparse = new SimpleDateFormat("dd/MMM/yyyy:HH:mm:ss");
    
	public static class hBaseReducer extends TableReducer <LongWritable, Text, ImmutableBytesWritable> {
		
		@Override
		public void reduce(LongWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
			for (Text val : values){
				try {
					Put a = LoadLogs.get_put(val.toString());
					context.write(null, a);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}			
		}
	}
	
	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new LoadLogsMR(), args);
		System.exit(res);
	}

	@Override
	public int run(String[] args) throws Exception {
		Configuration conf = HBaseConfiguration.create();
		Job job = Job.getInstance(conf, "read by MapReduce from bslc");
		job.setJarByClass(LoadLogsMR.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setReducerClass(hBaseReducer.class);

		job.setMapOutputKeyClass(LongWritable.class);
        job.setMapOutputValueClass(Text.class);
		
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(LongWritable.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		TextInputFormat.addInputPath(job, new Path(args[0]));
		TextOutputFormat.setOutputPath(job, new Path(args[1]));
		
		TableMapReduceUtil.addDependencyJars(job);
		TableMapReduceUtil.initTableReducerJob(args[1], hBaseReducer.class, job);
		job.setNumReduceTasks(3);

		return job.waitForCompletion(true) ? 0 : 1;
	}

}
