from pyspark import SparkConf, SparkContext
import sys, operator, re, string, json

inputs = sys.argv[1]
output = sys.argv[2]

conf = SparkConf().setAppName('reddit relative')
sc = SparkContext(conf=conf)

text = sc.textFile(inputs)
commentdata = text.map(lambda line: json.loads(line)).cache()

json_parsed = commentdata.map(lambda w: (w['subreddit'],(1, w['score'])))
commentbysub = commentdata.map(lambda c: (c['subreddit'], c))

#commentbysub.saveAsTextFile(output)
def add_pairs((a,b),(c,d)):
    return(a+c, b+d)

reducedData = json_parsed.reduceByKey(add_pairs)
averaged = reducedData.mapValues(lambda (a,b): 1.0*b/a)

withAverageScores = commentbysub.join(averaged)

author = withAverageScores.flatMap (lambda(a,(b,c)):[(1.0*b['score']/c, b['author'])])
sortedByRelScore = author.sortByKey(ascending=False).coalesce(1)

sortedByRelScore.saveAsTextFile(output)
