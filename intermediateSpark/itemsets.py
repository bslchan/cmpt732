from pyspark import SparkConf, SparkContext
from pyspark.mllib.fpm import FPGrowth

import sys, operator, re, string, json

inputs = sys.argv[1]
output = sys.argv[2]

conf = SparkConf().setAppName('freq itemsets mining')
sc = SparkContext(conf=conf)



text = sc.textFile(inputs)

transactions = text.flatMap(lambda line: [line.strip().split(' ')])

model = FPGrowth.train(transactions, 0.002, 8)

results = model.freqItemsets().map(lambda (a, b): (sorted(map(int,a)), b)).sortBy(lambda (a,b): a).sortBy(lambda (a,b): b, ascending=False).take(10000)

f=open ('myFile.txt', "w")
f.write("\n".join(map(lambda x: str(x), results)))
f.close()
