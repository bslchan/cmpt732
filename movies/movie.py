from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext, Row, functions as F
from pyspark.sql.functions import levenshtein, lit
from pyspark.mllib.recommendation import *
import sys, string

conf = SparkConf().setAppName('movie recommendations')
sc = SparkContext(conf=conf)
sqlContext = SQLContext(sc)

if (len(sys.argv) != 4):
    print " invalid number of inputs "
    sys.exit(1)

input = sys.argv[1]
userInput = sys.argv[2]
output = sys.argv[3]


def userRatingProcessing(inputLine):
    idx = inputLine.index(' ')
    return Row(rating=int(inputLine[:idx]), title=inputLine[idx+1:])

def twitterUserRatingProcessing(inputLine):
    fields = inputLine.split("::")
    return Row(user_id=fields[0], movie_id=fields[1], rating=fields[2])

def moviesProcessing(inputLine):
    fields = inputLine.split("::")
    return Row(movie_id=fields[0], movie_title=filter(lambda x: x in string.printable,  fields[1]))
    # return Row(movie_id=fields[0], movie_title=unicodedata.normalize('NFKD',  fields[1]))

def rowToList(row):
    return [int(row.user_id), int(row.movie_id), float(row.rating)]

def recommendationsToRow(r):
    return Row(movie_id=int(r.product))



#import personal (i.e., greg's) ratings
userData = sc.textFile(userInput)
userRatings = userData.map(userRatingProcessing)
userRatingsDF = sqlContext.createDataFrame(userRatings)
userRatingsDF.registerAsTable('userRatingsDF')

#import twitter users ratings
twitterUsersData = sc.textFile(input+"/ratings.dat")
ratings = twitterUsersData.map(twitterUserRatingProcessing)
twitterUsersRatings = twitterUsersData.map(twitterUserRatingProcessing)
twitterUsersRatingsDF = sqlContext.createDataFrame(twitterUsersRatings)

#import movie information
moviesDataRaw = sc.textFile(input+'/movies.dat')
moviesData = moviesDataRaw.map(moviesProcessing)
moviesDF = sqlContext.createDataFrame(moviesData).cache()
moviesDF.registerAsTable('moviesDF')

### cacluate Levenshtein distance
# first, join user and movie df
joinedTable = sqlContext.sql("select * from userRatingsDF cross join moviesDF")
# find levenshtein distance
distTable = joinedTable.select(joinedTable.movie_id, joinedTable.rating, joinedTable.title, joinedTable.movie_title, levenshtein(joinedTable.title, joinedTable.movie_title).alias('dist'))
# filter on the minimum distance
minOnlyTable = distTable.groupby('title').agg(F.min('dist').alias('dist'))
# only movie ID and ratings from user are necessary
ratingsOnlyTable = minOnlyTable.join(distTable, ['title', 'dist']).drop('title').drop('dist').drop('movie_title')
# provide user id #0 for the intended target.
userTable = ratingsOnlyTable.withColumn('user_id', lit(0))


#merge the user
table = userTable.unionAll(twitterUsersRatingsDF)
ratingsRDD = table.rdd
ratings = ratingsRDD.map(rowToList)


#build model
rank = 37
numIterations = 10
model = ALS.train(ratings, rank, numIterations)
#find recommendations
recommendations = model.recommendProducts(0, 10)

finalRecommendations = map(recommendationsToRow, recommendations)
finalRecommendationsDF = sqlContext.createDataFrame(finalRecommendations)

outputDF = finalRecommendationsDF.join(moviesDF, 'movie_id')
textOutput = outputDF.rdd

finalOutput = textOutput.map(lambda row: ("%s" %(row.movie_title)))
finalOutput.saveAsTextFile(output)


# ${SPARK_HOME}/bin/pyspark --master 'local[*]' movie.py input/ userRatings/ output/
