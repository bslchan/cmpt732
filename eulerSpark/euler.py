from pyspark import SparkContext, SparkConf
import sys
import random
from operator import add

inputs = sys.argv[1] #number of iterations run by the program
# output = sys.argv[2]

conf = SparkConf().setAppName('euler estimate')
sc = SparkContext(conf=conf)


numberOfPieces = 1000

generatingList = [int(inputs)/numberOfPieces]*numberOfPieces

rdd = sc.parallelize(generatingList, numberOfPieces)


def estimate_generator(n):
    rand = random.Random()
    count = 0
    count_counter = 0
    for i in xrange(n):
        total = 0
        while total < 1:
                    total += rand.random()
                    count += 1

    count_counter += count
    return count_counter


rdd2 = rdd.map(estimate_generator)

totalCount = rdd2.reduce(add)
finalEstimate = float(totalCount)/float(inputs)

print("The estimate for e is %f" %finalEstimate)


#3000000000