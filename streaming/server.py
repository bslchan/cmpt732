# Random data stream generator

import SocketServer, random, time, threading

# port and inter-observation delay pairs
port_intervals = [
    (10000, 1.0),
    (10001, 0.1),
    (10002, 0.01),
    (10003, 0.001),
    (10004, 0.0001),
    (10005, 0.00001),
    (10010, 0.0)]

max_run = 600 # Hang up after a certain time (but Spark Streaming will reconnect, so not much of a resource limiter.)

# the values we hope to reconstruct
m = 1
b = -1

# how much randomness do we add?
sample_sigma = 500
fuzz_sigma = 5

def data_point(rand):
    """
    Return a random x,y data point near our data line.
    """
    x = rand.gauss(0, sample_sigma)
    y = m*x + b + rand.gauss(0, fuzz_sigma)
    return x, y

def DataTCPHandler(port, interval):
    """
    Build RequestHandler instance to use.
    """
    # closure over port and interval so they're known to the RequestHandler class.
    class TheTCPHandler(SocketServer.BaseRequestHandler):
        def handle(self):
            print "%s on port %i with observation interval %g s." % (self.client_address[0], port, interval)
            start = time.time()
            rand = random.Random() # docs say threadsafe, but construct one per thread anyway

            while time.time() - start < max_run:
                x, y = data_point(rand)
                self.request.sendall('%f %f\n' % (x, y))
                time.sleep(interval)

    return TheTCPHandler

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass

if __name__ == "__main__":
    # multi-port listening recipe adapted from http://stackoverflow.com/a/3655592/1236542
    for port, interval in port_intervals:
        server = ThreadedTCPServer(('', port), DataTCPHandler(port, interval))
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.setDaemon(True)
        server_thread.start()

    while 1:
        time.sleep(1)