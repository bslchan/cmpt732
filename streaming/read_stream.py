from pyspark import SparkContext
from pyspark.streaming import StreamingContext
import sys, datetime

output = sys.argv[1]

# Create a local StreamingContext with two working threads and batch interval of 1 second
sc = SparkContext("local[2]", "NetworkWordCount")
ssc = StreamingContext(sc, 1)

# Create a DStream that will connect to hostname:port, like localhost:9999
# dStream = ssc.socketTextStream("cmpt732.csil.sfu.ca", 10001) # 10 pts/ second
dStream = ssc.socketTextStream("cmpt732.csil.sfu.ca", 10003) # 1000 pts/ second
# dStream = ssc.socketTextStream("cmpt732.csil.sfu.ca", 10005) # 100,000 pts/ second
# dStream = ssc.socketTextStream("cmpt732.csil.sfu.ca", 10010) # 350k pts/ second

def lineProcessing(line):
    coords = line.strip().split(" ")
    return (1, float(coords[0]), float(coords[1]), float(coords[0])*float(coords[0]), float(coords[0])*float(coords[1]))

def add_tuples(a, b):
    return tuple(sum(p) for p in zip(a,b))

# def find_slope(n, Sx, Sy, Sx2, Sxy):
def find_slope(tuple):
    xmean = tuple[1]/tuple[0]
    ymean = tuple[2]/tuple[0]
    xymean = tuple[4]/tuple[0]
    x2mean = tuple[3]/tuple[0]

    slope = (xymean-xmean*ymean)/(x2mean-xmean*xmean)
    intercept = ymean-slope*xmean

    return (slope, intercept, tuple[0])

coords = dStream.map(lineProcessing)
reduced = coords.reduce(add_tuples)
info = reduced.map(find_slope)

info.pprint()
info.foreachRDD(lambda rdd: rdd.saveAsTextFile(output + '/' + datetime.datetime.now().isoformat().replace(':', '-')))


ssc.start()
ssc.awaitTermination(timeout=300)