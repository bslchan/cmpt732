
// adapted from https://hadoop.apache.org/docs/current/hadoop-mapreduce-client/hadoop-mapreduce-client-core/MapReduceTutorial.html
//current program counts the number of wikiepdia article in english and their page requests. maoutputs filename, # of page hit

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


import java.io.*;

public class RedditAverage extends Configured implements Tool {

	public static class jsonMapper extends Mapper<LongWritable, Text, Text, LongPairWritable> {

		private long one = 1;
		private long score;
		private LongPairWritable pair = new LongPairWritable();
		private Text subReddit = new Text();
		

		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

			ObjectMapper json_mapper = new ObjectMapper();
			String line = value.toString();

			JsonNode data = json_mapper.readValue(line, JsonNode.class);
			subReddit.set(data.get("subreddit").textValue());
			score = data.get("score").longValue();
			
			pair.set(one, score);
			context.write(subReddit, pair);
			
		}
	}

public static class redditCombiner extends Reducer<Text, LongPairWritable, Text, LongPairWritable> {
		
		private LongPairWritable result = new LongPairWritable();
		
		@Override
		public void reduce(Text key, Iterable<LongPairWritable> pairs, Context context) throws IOException, InterruptedException {
			long sum_0 = 0;
			long sum_1 = 0;
			for (LongPairWritable val : pairs) {
					sum_0 +=val.get_0();
					sum_1 +=val.get_1();
			}
			
			result.set(sum_0, sum_1);
			context.write(key, result);
		}
	}
	
    public static class redditReducer extends Reducer<Text, LongPairWritable, Text, DoubleWritable> {
		
		private DoubleWritable average = new DoubleWritable();
		
			
		@Override
		public void reduce(Text key, Iterable<LongPairWritable> pairs, Context context) throws IOException, InterruptedException {
			long sum_subReddit_count =0;
			long sum_Score = 0;
			for (LongPairWritable val : pairs) {
				sum_subReddit_count +=val.get_0();
				sum_Score +=val.get_1();
			}
			
			double subReddit_count_double = (long) sum_subReddit_count;
			double score_double = (long) sum_Score;
			
			average.set(score_double/subReddit_count_double);
						
			context.write(key, average);
		}
	}

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new RedditAverage(), args);
		System.exit(res);
	}

	@Override
	public int run(String[] args) throws Exception {
		Configuration conf = this.getConf();
		Job job = Job.getInstance(conf, "word count");
		job.setJarByClass(RedditAverage.class);

		job.setInputFormatClass(MultiLineJSONInputFormat.class);

		job.setMapperClass(jsonMapper.class);
		job.setCombinerClass(redditCombiner.class); //comment to test whether combiner makes it faster 
		job.setReducerClass(redditReducer.class);

		job.setOutputKeyClass(Text.class);
		job.setMapOutputValueClass(LongPairWritable.class);
		job.setOutputValueClass(DoubleWritable.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		TextInputFormat.addInputPath(job, new Path(args[0]));
		TextOutputFormat.setOutputPath(job, new Path(args[1]));

		return job.waitForCompletion(true) ? 0 : 1;
	}

}