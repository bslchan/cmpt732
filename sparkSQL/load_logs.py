from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext, Row, DataFrame
from datetime import datetime
import sys, re, unicodedata

inputs = sys.argv[1]
output = sys.argv[2]

conf = SparkConf().setAppName('correlates logs better')
sc = SparkContext(conf=conf)
sqlContext = SQLContext(sc)
text = sc.textFile(inputs)

linere = re.compile("^(\\S+) - - \\[(\\S+) [+-]\\d+\\] \"[A-Z]+ (\\S+) HTTP/\\d\\.\\d\" \\d+ (\\d+)$")


def lineProcessing(inputLine):
    if linere.match(inputLine):
        m = linere.match(inputLine)
        return [Row(host=unicodedata.normalize('NFKD',m.group(1)), date=datetime.strptime(m.group(2),'%d/%b/%Y:%H:%M:%S'), path=unicodedata.normalize('NFKD',m.group(3)), bytes=float(unicodedata.normalize('NFKD',m.group(4))))]
    return []

rawLogs = text.flatMap(lineProcessing)
df = sqlContext.createDataFrame(rawLogs)
df.write.format('parquet').save(output)

reinput = sqlContext.read.parquet(output)
sums = reinput.select('host', 'bytes').groupby('host').sum()
sums.show()