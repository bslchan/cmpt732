from pyspark import SparkConf, SparkContext
from pyspark.sql.readwriter import DataFrameWriter
from pyspark.sql.types import StructType, StructField, StringType, IntegerType
from pyspark.sql import SQLContext
import sys, operator, re, string, json


inputs = sys.argv[1]
output = sys.argv[2]

conf = SparkConf().setAppName('reddit averages by sparkSQL')
sc = SparkContext(conf=conf)
sqlContext = SQLContext(sc)
schema = StructType([ StructField('subreddit', StringType(), False),StructField('score', IntegerType(), False)])

comments = sqlContext.read.json(inputs, schema=schema)
averages = comments.select('subreddit', 'score').groupby('subreddit').avg()
averages.show()
averages.write.save(output, format='json', mode='overwrite')


#
#
#
# text = sc.textFile(inputs)
#
# json_parsed = text.map(lambda line: json.loads(line)).map(lambda w: (w['subreddit'],(1, w['score'])))
#
# def add_pairs((a,b),(c,d)):
#     return(a+c, b+d)
#
# reducedData = json_parsed.reduceByKey(add_pairs)
# averaged = reducedData.mapValues(lambda (a,b): 1.0*b/a)
# outdata = averaged.sortBy(lambda (w,c): w).map(lambda (w,c): u"[\"%s\", %.16f]" % (w, c)).coalesce(1)
# outdata.saveAsTextFile(output)
