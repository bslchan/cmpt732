from pyspark import SparkConf, SparkContext
import sys, math, re, string, json

inputs = sys.argv[1]
output = sys.argv[2]

conf = SparkConf().setAppName('correlates logs better')
sc = SparkContext(conf=conf)

text = sc.textFile(inputs)

linere = re.compile("^(\\S+) - - \\[(\\S+) [+-]\\d+\\] \"[A-Z]+ (\\S+) HTTP/\\d\\.\\d\" \\d+ (\\d+)$")


def lineProcessing(inputLine):
    if linere.match(inputLine):
        m = linere.match(inputLine)
        return [[m.group(1), m.group(4)]]
    return []

def customMap(input):
    return (input[0], (1, int(input[1])))


rawLogs = text.flatMap(lineProcessing).map(customMap)


def add_pairs((a, b), (c, d)):
    return (a + c, b + d)

reducedLogs = rawLogs.reduceByKey(add_pairs)

def add_tuples(a, b):
    return tuple(sum(p) for p in zip(a,b))

(n,x,y, x2, y2, xy) = reducedLogs.map(lambda (a,(b,c)): (1, float(b),float(c), float(b*b), float(c*c), float(b*c))).reduce(add_tuples)

xavg = x/n
yavg = y/n

(numer, denom1, denom2) = reducedLogs.map(lambda (a,(b,c)): ((b-xavg)*(c-yavg), (b-xavg)**2, (c-yavg)**2)).reduce(add_tuples)


r = numer/math.sqrt(denom1)/math.sqrt(denom2)

print ("r = "+str(r))
print ("r**2 = "+str(r*r))




