from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.types import *
import sys

conf = SparkConf().setAppName('weather')
sc = SparkContext(conf=conf)
sqlContext = SQLContext(sc)

inputs = sys.argv[1]
output = sys.argv[2]

fields = [StructField('station', StringType(), True),
          StructField('date', StringType(), True),
          StructField('element', StringType(), True),
          StructField('value', IntegerType(), True),
          StructField('mflag', StringType(), True),
          StructField('qflag1', StringType(), True),
          StructField('SFLAG1', StringType(), True),
          StructField('VALUE2', IntegerType(), True)
          ]
customSchema = StructType(fields)

df = sqlContext.read.format('com.databricks.spark.csv').options(header=True).load(inputs, schema=customSchema).cache()
df.registerTempTable("df")

max = sqlContext.sql("select date, station, value from df where qflag1 ='' and element = 'TMAX'")
min = sqlContext.sql("select date, station, value from df where qflag1 ='' and element = 'TMIN'")

max.registerTempTable("max")
min.registerTempTable("min")

joined = sqlContext.sql("select max.date, max.station, max.value as tMax, min.value as tMin "
                        "from max "
                        "inner join min "
                        "on max.date=min.date and max.station=min.station")
joined.registerTempTable('joined')

tRange = sqlContext.sql ("select joined.date, joined.station, tMax-tMin as range from joined")
tRange.registerTempTable('tRange')

filtered = sqlContext.sql("select tRange.date, max(tRange.range) as range from tRange group by tRange.date "
                          "order by tRange.date")
filtered.registerTempTable('filtered')

reduced = sqlContext.sql("select tRange.date, tRange.station, filtered.range "
                         "from tRange "
                         "right join filtered "
                         "on filtered.range=tRange.range and filtered.date=tRange.date "
                         "order by tRange.date")
reduced.registerTempTable('reduced')

final = sqlContext.sql("select distinct reduced.date, reduced.station, reduced.range from reduced")

textOutput = final.rdd
finalOutput = textOutput.map(lambda row: (u"%s %s %s" %(row.date, row.station, row.range)))
finalOutput.saveAsTextFile(output)

#rm -r weatherOut/ && ${SPARK_HOME}/bin/spark-submit --master "local[*]" --packages com.databricks:spark-csv_2.11:1.2.0 temp_range_sql.py a4-weather-1/ weatherOut