from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext, functions as F
from pyspark.sql.types import *
import sys

conf = SparkConf().setAppName('weather')
sc = SparkContext(conf=conf)
sqlContext = SQLContext(sc)

inputs = sys.argv[1]
output = sys.argv[2]

fields = [StructField('station', StringType(), True),
          StructField('date', StringType(), True),
          StructField('element', StringType(), True),
          StructField('value1', IntegerType(), True),
          StructField('MFLAG1', StringType(), True),
          StructField('QFLAG1', StringType(), True),
          StructField('SFLAG1', StringType(), True),
          StructField('VALUE2', IntegerType(), True)
          ]
customSchema = StructType(fields)

df = sqlContext.read.format('com.databricks.spark.csv').options(header=True).load(inputs, schema=customSchema)

df = df[df.QFLAG1 == ''] #dropped rows that have entries with QFLAG attribute
df = df[['station', 'date', 'element', 'value1']] #drop uncessary columns

df2 = df[(df.element == 'TMAX')]['station', 'date', 'value1'] #take only rows with tmax
df2 = df2.withColumnRenamed('value1', 'max')

df3 = df[(df.element == 'TMIN')]['station', 'date', 'value1']
df3 = df3.withColumnRenamed('value1', 'min') #take only rows with tmin

df4 = df2.join(df3, ['station', 'date']) #join by statoin and date
df5 = df4.withColumn('range', df4.max-df4.min)['date', 'station', 'range'] #find range
df6 = df5.groupBy('date').agg(F.max('range').alias('range'))


df7 = df6.join(df5, [df6.date == df5.date, df6.range == df5.range]).drop(df5.range).drop(df5.date)['date','station','range'].sort('date', ascending=True)

textOutput = df7.rdd
finalOutput = textOutput.map(lambda row: (u"%s %s %s" %(row.date, row.station, row.range)))
finalOutput.saveAsTextFile(output)

# ${SPARK_HOME}/bin/spark-submit --master "local[*]" --packages com.databricks:spark-csv_2.11:1.2.0 temp_range.py a4-weather-1/ weatherOut