from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext, Row, functions as F, DataFrame
import sys

conf = SparkConf().setAppName('dijkstra')
sc = SparkContext(conf=conf)
sqlContext = SQLContext(sc)

inputs = sys.argv[1]
output = sys.argv[2]
startingNode = int(sys.argv[3])
endingNode = int(sys.argv[4])

#process the input lines to create the digraph
text = sc.textFile(inputs)
def lineProcessing(inputLine):
    splitStr = inputLine.strip().split(" ")
    if len(splitStr) == 1:
        return [Row(source=int(splitStr[0][:len(splitStr[0])-1]), dest=0)]
    elif len(splitStr) > 1:
        seq = [] #seq of Rows
        for k in range(1, len(splitStr)):
            seq.append(Row(source=int(splitStr[0][:len(splitStr[0])-1]), dest=int(splitStr[k])))
        return seq

inputGraph = text.flatMap(lineProcessing)

inputGraphDf = sqlContext.createDataFrame(inputGraph)
inputGraphDf.cache()

def printIteration(row):
    if row.source == 0:
        tempStr = "node %i: no source node, distance %i" %(row.node, row.dist)
    else:
        tempStr = "node %i: source %i, distance %i" %(row.node, row.source, row.dist)
    return tempStr



for i in range(8):
    if i == 0:
        initialRow = [Row(node=int(startingNode), source=0, dist=0)]
        paths = sqlContext.createDataFrame(initialRow)['node', 'source', 'dist']
        paths.cache()
        writeOutput = paths.rdd.map(printIteration)
        writeOutput.saveAsTextFile(output + '/iter-' + str(i))
    else:
        if paths.filter(paths.node == endingNode).count() == 0:
            tempPaths = paths.join(inputGraphDf, inputGraphDf.source == paths.node, 'inner')[inputGraphDf.dest, inputGraphDf.source]
            tempPaths1 = tempPaths.withColumnRenamed('dest', 'node').withColumn('dist', F.lit(i))
            paths = paths.unionAll(tempPaths1).dropDuplicates(['node']).filter(paths.node != 0).sort('dist', ascending=True)
            paths.cache()
            writeOutput = paths.rdd.map(printIteration)
            writeOutput.saveAsTextFile(output + '/iter-' + str(i))

if paths.filter(paths.node == endingNode).count() != 0:
    pathList=[int(endingNode)]
    sourceNode = int(paths.filter(paths.node == endingNode).first().source)
    pathList.append(sourceNode)
    currentDist  =  int(paths.filter(paths.node == endingNode).first().dist)-1

    while sourceNode!= startingNode:
        sourceNode = int(paths.filter(paths.node == sourceNode).first().source)
        pathList.append(sourceNode)
        currentDist+=-1

    finalpath = sc.parallelize(pathList[::-1])
else:
    finalpath = sc.parallelize(["path not found"])

finalpath.saveAsTextFile(output + '/path')