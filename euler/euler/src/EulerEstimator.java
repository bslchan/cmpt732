
// adapted from https://hadoop.apache.org/docs/current/hadoop-mapreduce-client/hadoop-mapreduce-client-core/MapReduceTutorial.html
//current program counts the number of wikiepdia article in english and their page requests. maoutputs filename, # of page hit

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.NullOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import java.util.Random;

import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.*;

public class EulerEstimator extends Configured implements Tool {

	public static class EulerMapper extends Mapper<LongWritable, Text, Text, LongWritable> {
		long iterations_count;
		long count_counter;
		
		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

			int	iter = Integer.parseInt(value.toString());
			int count =0;
			double sum;
			
			
			iterations_count=0;
			count_counter=0;
			
			context.getInputSplit();
			String fileName = ((FileSplit) context.getInputSplit()).getPath().getName();
			long seed = fileName.hashCode()+key.get();
			
			Random rnd = new Random();
			rnd.setSeed(seed);
			
			for (int i=1;i<=iter;i++){
				sum=0;
				while (sum<1){
					sum += rnd.nextDouble();
					count++;
				}
			}
			iterations_count +=iter;
			count_counter+=count;
			context.getCounter("Euler", "iterations").increment(iterations_count);
			context.getCounter("Euler", "count").increment(count_counter);
			
			
		}
	}



	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new EulerEstimator(), args);
		System.exit(res);
	}

	@Override
	public int run(String[] args) throws Exception {
		Configuration conf = this.getConf();
		Job job = Job.getInstance(conf, "euler estimator by bslc");
		job.setJarByClass(EulerEstimator.class);

		job.setInputFormatClass(TextInputFormat.class);

		job.setMapperClass(EulerMapper.class);

//		job.setOutputKeyClass(Text.class);
//		job.setOutputValueClass(LongWritable.class);
		job.setOutputFormatClass(NullOutputFormat.class);
		TextInputFormat.addInputPath(job, new Path(args[0]));
		TextOutputFormat.setOutputPath(job, new Path(args[1]));

		return job.waitForCompletion(true) ? 0 : 1;
	}
}