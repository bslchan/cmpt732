from pyspark import SparkConf, SparkContext
import sys, operator, re, string, json

inputs = sys.argv[1]
output = sys.argv[2]

conf = SparkConf().setAppName('reddit averages')
sc = SparkContext(conf=conf)

text = sc.textFile(inputs)

json_parsed = text.map(lambda line: json.loads(line)).map(lambda w: (w['subreddit'],(1, w['score'])))

def add_pairs((a,b),(c,d)):
    return(a+c, b+d)

reducedData = json_parsed.reduceByKey(add_pairs)
averaged = reducedData.mapValues(lambda (a,b): 1.0*b/a)
outdata = averaged.sortBy(lambda (w,c): w).map(lambda (w,c): u"[\"%s\", %.16f]" % (w, c)).coalesce(1)
outdata.saveAsTextFile(output)
