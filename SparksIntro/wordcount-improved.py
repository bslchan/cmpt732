from pyspark import SparkConf, SparkContext
import sys, operator, re, string, unicodedata

inputs = sys.argv[1]
output = sys.argv[2]

conf = SparkConf().setAppName('word count')
sc = SparkContext(conf=conf)

text = sc.textFile(inputs)

wordsep = re.compile(r'[%s\s]+' % re.escape(string.punctuation))

words = text.flatMap(lambda line: wordsep.split(unicodedata.normalize('NFD', line.lower()))).filter(lambda line: line.strip() != '' and line).map(lambda w: (w, 1))

wordcount = words.reduceByKey(operator.add).cache()

outdata = wordcount.sortBy(lambda (w,c): w).map(lambda (w,c): u"%s %i" % (w, c)).coalesce(1)
outdata.saveAsTextFile('by-word')

outdata = wordcount.sortBy(lambda (w,c): c).map(lambda (w,c): u"%s %i" % (w, c)).coalesce(1)
outdata.saveAsTextFile('by-freq')